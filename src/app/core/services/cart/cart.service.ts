import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';
import { Product } from '../../../model/product.model';


@Injectable({
  providedIn: 'root'
})
export class CartService {

  private productsSlected: Product[] = [];

  // le cart commence avec 0 products.
  private cart = new BehaviorSubject<Product[]>([]);

  // var utilisee pour n'importe quelle composant
  // et a qui on peut ce subscribe.
  cart$ = this.cart.asObservable();

  constructor() { }

  // Pattern Observer
  addCart(productParam: Product) {
    // on cree un nouveau d'etat de l'array
    // no MUTABLE a l'array. pas utilisation du push.
    this.productsSlected = [...this.productsSlected, productParam];
    
    // Notifie a tous les components qui sont sousrit qu'il y a eu un changement.
    // le nouveau etat de mon array
    this.cart.next(this.productsSlected);
  }
}

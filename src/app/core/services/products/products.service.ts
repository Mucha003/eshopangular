import { Injectable } from '@angular/core';
import { Product } from '../../../model/product.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  baseUrl = `${ environment.urlApi }/products`;

  constructor(
    private http: HttpClient
  ) { }

  getAllProducts() {
    return this.http.get<Product[]>(this.baseUrl);
  }

  getProduct(id: string) {
    return this.http.get<Product>(`${ this.baseUrl }/${id}`);
  }

  addProduct(product: Product) {
    return this.http.post(this.baseUrl, product);
  }

  updateProduct(id: string, productCange: Partial<Product>) {
    return this.http.put(`${ this.baseUrl }/${id}`, productCange);
  }

  deleteProduct(id: string) {
    return this.http.delete(`${ this.baseUrl }/${id}`);
  }
}

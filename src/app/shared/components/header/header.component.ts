import { Component, OnInit } from '@angular/core';
import { CartService } from '../../../core/services/cart/cart.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  // Les Observable vont gerer mieux les subscribe.
  // On va ce subcribe dans le template Html.
  totalItems$: Observable<number>;

  constructor(
    private cartService: CartService,
  ) {
    // ecoute cette observable.
    // pour tous les flux des données(reactive) le catch avec un pipe
    // map() => utliser pour transformer(une valuer pour un nouveau).
    this.totalItems$ = this.cartService.cart$.pipe(map(x => x.length));
  }

  ngOnInit() { }

}

import { AbstractControl } from '@angular/forms';

export class MyCustomFormValidators {
    static ispriceValid(control: AbstractControl) {
        console.log(control.value);

        if (control.value > 1000) {
            // renvoi un Obj json avec l'erreur
            return { price_Invalid: true };
        }
        // null s'il n'y a pas d'erreur.
        return null;
    }
}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from 'src/app/model/product.model';
import { CartService } from '../../../../core/services/cart/cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  @Input() product: Product;
  @Output() itemToChart: EventEmitter<any> = new EventEmitter();

  constructor(
    private cartService: CartService
  ) { }

  ngOnInit() {
  }

  addToChart() {
    this.cartService.addCart(this.product);
    // this.itemToChart.emit(this.product);
  }

}

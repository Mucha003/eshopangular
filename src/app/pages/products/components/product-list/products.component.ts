import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/product.model';
import { ProductsService } from '../../../../core/services/products/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  productList: Product[] = [];

  constructor(
    private productService: ProductsService
  ) { }

  ngOnInit() {
    this.getAllProducts();
  }

  addProductToChart(product: Product) {
    console.log(product);
  }

  getAllProducts() {
    this.productService.getAllProducts().subscribe((x: Product[]) => {
      this.productList = x;
      console.log(x);
    });
  }

}

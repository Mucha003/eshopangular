import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductsService } from 'src/app/core/services/products/products.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MyCustomFormValidators } from 'src/app/utils/validators';
import { Product } from '../../../../model/product.model';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {

  productFG: FormGroup;
  id: string;

  constructor(
    private fb: FormBuilder,
    private productsService: ProductsService,
    private router: Router,
    private activatedRoute: ActivatedRoute
    ) {
      this.InitForm();
    }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((param: Params) => {
      this.id = param.id;
      console.log(this.id);
      this.productsService.getProduct(this.id).subscribe((product: Product) => {
        console.log(product);
        this.productFG.patchValue({ product });
      });
    });
  }

  private InitForm() {
    this.productFG = this.fb.group({
      title: ['', [ Validators.required ]],
      price: [0, [ Validators.required, MyCustomFormValidators.ispriceValid]],
      image: [''],
      description: ['', [ Validators.required ]],
    });
  }

  get priceformField() {
    // ceci est la mm chose que si on
    // le fessait au niveau html pour eviter
    // la repetion du code.
    return this.productFG.get('price');
  }

  SaveProduct(event: Event) {
    // enleve le comportement par default.
    event.preventDefault(); // evite de recharger la page.
    console.log(this.productFG.value);

    if (this.productFG.valid) {
      this.productsService.updateProduct(this.id, this.productFG.value)
      .subscribe((product) => {
        console.log(product);
        this.router.navigate(['products']);
      });
    }
  }

}

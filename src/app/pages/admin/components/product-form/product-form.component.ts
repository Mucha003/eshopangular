import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,  Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductsService } from 'src/app/core/services/products/products.service';
import { MyCustomFormValidators } from '../../../../utils/validators';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  productFG: FormGroup;

  constructor(
    private fb: FormBuilder,
    private productsService: ProductsService,
    private router: Router
    ) {
      this.InitForm();
    }

  ngOnInit(): void { }

  private InitForm() {
    this.productFG = this.fb.group({
      id: ['', [ Validators.required ]],
      title: ['', [ Validators.required ]],
      price: [0, [ Validators.required, MyCustomFormValidators.ispriceValid]],
      image: [''],
      description: ['', [ Validators.required ]],
    });
  }

  get priceformField() {
    // ceci est la mm chose que si on
    // le fessait au niveau html pour eviter
    // la repetion du code.
    return this.productFG.get('price');
  }

  SaveProduct(event: Event) {
    // enleve le comportement par default.
    event.preventDefault(); // evite de recharger la page.
    console.log(this.productFG.value);

    if (this.productFG.valid) {
      this.productsService.addProduct(this.productFG.value)
      .subscribe((product) => {
        console.log(product);
        this.router.navigate(['products']);
      });
    }
  }
}

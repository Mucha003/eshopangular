import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/product.model';
import { CartService } from 'src/app/core/services/cart/cart.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  // Avec sa on ne ce subscribe pas
  productsCart$: Observable<Product[]>;

  constructor(private cartService: CartService) {
    this.productsCart$ = this.cartService.cart$;
   }

  ngOnInit() {
  }

}

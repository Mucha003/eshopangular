import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules, PreloadingStrategy } from '@angular/router';
import { LayoutComponent } from './components/layout/layout.component';
import { AdminGuard } from './admin.guard';


const routes: Routes = [
  {
    path: '', component: LayoutComponent, children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        canActivate: [AdminGuard],
        loadChildren: () => import('../app/pages/home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'products',
        canActivate: [AdminGuard],
        loadChildren: () => import('../app/pages/products/products.module').then(m => m.ProductsModule)
      },
      {
        path: 'contact',
        canActivate: [AdminGuard],
        loadChildren: () => import('../app/pages/contact/contact.module').then(m => m.ContactModule)
      },
      {
        path: 'order',
        canActivate: [AdminGuard],
        loadChildren: () => import('../app/pages/order/order.module').then(m => m.OrderModule)
      }]
  },
  {
    path: 'admin',
    loadChildren: () => import('../app/pages/admin/admin.module').then(m => m.AdminModule)
  },
  {
    path: '**',
    loadChildren: () => import('../app/pages/page-not-found/page-not-found.module').then(m => m.PageNotFoundModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
